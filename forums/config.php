<?php return array (
  'debug' => false,
  'database' => 
  array (
    'driver' => 'mysql',
    'host' => 'localhost',
    'port' => 3306,
    'database' => 'Dev_Connect',
    'username' => 'root',
    'password' => 'venkatesh',
    'charset' => 'utf8mb4',
    'collation' => 'utf8mb4_unicode_ci',
    'prefix' => '',
    'strict' => false,
    'engine' => 'InnoDB',
    'prefix_indexes' => true,
  ),
  'url' => 'http://localhost/forums/public',
  'paths' => 
  array (
    'api' => 'api',
    'admin' => 'admin',
  ),
);